unit Udatos;

interface

uses
  SysUtils,  pFIBDatabase, pFIBQuery, IniFiles, Classes, StrUtils, ShellAPI, WinSvc, Windows, WinSock,
  IdHTTP, IdSSLOpenSSL, Messages, IdHashMessageDigest, TlHelp32, uLkJSON, base64, IdMultipartFormData;

type
  Datos = class
  private
    { Private declarations }
  public
    { Public declarations }
    ruta, ruta_utilidades: string;
    terminado: Boolean;
    Count: Integer;
    baseDatos: TpFIBDatabase;
    transaccion: TpFIBTransaction;
    query: TpFIBQuery;
    QS: TpFIBQuery;
    transQS: TpFIBTransaction;
    fichero_config: TIniFile;
    maquina, ruta_datos, ruta_datos_nueva, ruta_carpeta_nueva, nombre_folder, numCliente: string;
    archivo: TextFile;
    isSaas: Boolean;
    licencia: string;
    GB_Padre: string;

    procedure init;
    procedure abrirDb;
    procedure cerrarDb;
    procedure ejecuta_query(sql:String);
    procedure pararServicioGlop;
    procedure getBackup( nombre: string );
    procedure listBackup;

    function cargarLic: Boolean;
    function isGlopActive: Boolean;
    function cloneDb: Boolean;
    function tarBd: Boolean;
    function unTarBd( nombre: string) : Boolean;
    function saveBackup: Boolean;
    function gfixBd: Boolean;
    function setGlopIni: Boolean;
    function copyLocalBd: Boolean;
    function restoreBd: Boolean;
    function actualizarIni: Boolean;
    function launchGlop: Boolean;
    function escribirFlag: Boolean;


    procedure subirFileGordo;

  const
    TAM_SAAS:Integer                 = 1;
    INICIO_SAAS:Integer              = 2821;
    FIN_SAAS:Integer                 = 2822;
    TAM_MATRICULA: Integer = 15;
    INICIO_MATRICULA: Integer = 600;
    FIN_MATRICULA: Integer = 615;

    TAM_PADRE:Integer  = 15;
    INICIO_PADRE:Integer = 2803;
    FIN_PADRE:Integer    = 2818;

    URL_ENDPOINT: string = 'https://20190307t173944-dot-glop-cloud-belgica.appspot.com/api/backup';
    URL_ENDPOINT_GLOP: string = 'https://www.glop.es/google/index-descarga.php';
    
  end;

implementation

// FUNCI�N INTERNA: ELIMINAR CARACTERES
function eliminar_caracteres(cad: string; cambiar: char): string;
begin
  while pos(cambiar, cad) > 0 do
  begin
    Delete(cad, pos(cambiar, cad), 1);
  end;
  result := cad;
end;

// FUNCI�N INTERNA: DESENCRIPTAR VALOR DE LICENCIA
function desencrip(aStr: string; aKey: Integer): string;
begin
  Result := '';
  RandSeed := aKey;
  for aKey := 1 to Length(aStr) do
    Result := Result + Chr(Byte(aStr[aKey]) xor random(256));
end;

// FUNCI�N INTERNA: PARAR UN SERVICIO
procedure pararServicio(nombre: string);
var
  Scm, Svc: SC_Handle;
  Status: SERVICE_STATUS;
begin
  Scm := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if Scm <> 0 then
  begin
    Svc := OpenService(Scm, PChar(nombre), SERVICE_ALL_ACCESS);
    if Svc <> 0 then
    begin
      ControlService(Svc, SERVICE_CONTROL_STOP, Status);
      // handle Status....
      CloseServiceHandle(Svc);
    end;
    CloseServiceHandle(Scm);
  end;
end;

// FUNCI�N INTERNA: OBTENER EL ESTADO DE UN SERVICIO
function ServiceGetStatus(sService: PChar): DWORD;
var
  SCManHandle, SvcHandle: SC_Handle;
  SS: TServiceStatus;
  dwStat: DWORD;
begin
  dwStat := 0;
  // Open service manager handle.
  SCManHandle := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);
  if (SCManHandle > 0) then
  begin
    SvcHandle := OpenService(SCManHandle, sService, SERVICE_QUERY_STATUS);
    // if Service installed
    if (SvcHandle > 0) then
    begin
      // SS structure holds the service status (TServiceStatus);
      if (QueryServiceStatus(SvcHandle, SS)) then
        dwStat := SS.dwCurrentState;
      CloseServiceHandle(SvcHandle);
    end;
    CloseServiceHandle(SCManHandle);
  end;
  Result := dwStat;
end;

// FUNCI�N INTERNA: EJECUTAR COMANDO CONSOLA
function GetDosOutput(CommandLine: string; Work: string = 'C:\'): string;
var
  SecAtrrs: TSecurityAttributes;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  StdOutPipeRead, StdOutPipeWrite: THandle;
  WasOK: Boolean;
  pCommandLine: array[0..255] of AnsiChar;
  BytesRead: Cardinal;
  WorkDir: string;
  Handle: Boolean;
begin
  Result := '';
  with SecAtrrs do begin
    nLength := SizeOf(SecAtrrs);
    bInheritHandle := True;
    lpSecurityDescriptor := nil;
  end;
  CreatePipe(StdOutPipeRead, StdOutPipeWrite, @SecAtrrs, 0);
  try
    with StartupInfo do
    begin
      FillChar(StartupInfo, SizeOf(StartupInfo), 0);
      cb := SizeOf(StartupInfo);
      dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
      wShowWindow := SW_HIDE;
      hStdInput := GetStdHandle(STD_INPUT_HANDLE); // don't redirect stdin
      hStdOutput := StdOutPipeWrite;
      hStdError := StdOutPipeWrite;
    end;
    WorkDir := Work;
    Handle := CreateProcess(nil, PChar('cmd.exe /C ' + CommandLine),
                            nil, nil, True, 0, nil,
                            PChar(WorkDir), StartupInfo, ProcessInfo);
    CloseHandle(StdOutPipeWrite);
    if Handle then
      try
        repeat
          WasOK := windows.ReadFile(StdOutPipeRead, pCommandLine, 255, BytesRead, nil);
          if BytesRead > 0 then
          begin
            pCommandLine[BytesRead] := #0;
            Result := Result + pCommandLine;
          end;
        until not WasOK or (BytesRead = 0);
        WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
      finally
        CloseHandle(ProcessInfo.hThread);
        CloseHandle(ProcessInfo.hProcess);
      end;
  finally
    CloseHandle(StdOutPipeRead);
  end;
end;

// FUNCI�N INTERNA: POSTEAR A UNA URL UN JSON
function makeCall(Url:string;valorAEnviar: string): widestring;
var
  lHTTP: TIdHTTP;
  parametros: TStream;
  str: TMemoryStream;
  openSSL: TIdSSLIOHandlerSocketOpenSSL;

  function NoLineFeed(const s: WideString): WideString;
  begin
    result := s;
    result := AdjustLineBreaks(result, tlbsCRLF);
    result := AnsiReplaceStr(result, #13#10, '\n');
  end;

  function StreamToString(Stream: TStream): WideString;
  begin
    with TStringStream.Create('') do
    try
      CopyFrom(Stream, Stream.Size - Stream.Position);
      Result := DataString;
    finally
      Free;
    end;
  end;

begin
  str := TMemoryStream.Create;
//  Form1.Memo1.Lines.Add('Se va a enviar: '+valorAEnviar);
  parametros := TMemoryStream.Create;
  openSSL := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
  openSSL.SSLOptions.Method := sslvTLSv1_2;
  openSSL.SSLOptions.SSLVersions := [sslvSSLv2, sslvSSLv3, sslvTLSv1, sslvTLSv1_1, sslvTLSv1_2, sslvSSLv23];
  openSSL.SSLOptions.Mode := sslmUnassigned;

  valorAEnviar := NoLineFeed(valorAEnviar);
 //Esto es para escribir un string (json) a un Tstream
  parametros.WriteBuffer(Pointer(valorAEnviar)^, length(valorAEnviar));
  parametros.Position := 0;
  SetLength(valorAEnviar, parametros.Size);
  parametros.ReadBuffer(Pointer(valorAEnviar)^, parametros.Size);
  parametros.Position := 0;

  lHTTP := TIdHTTP.Create;
  lHTTP.AllowCookies := True;
  lHTTP.HTTPOptions := [hoKeepOrigProtocol, hoInProcessAuth, hoForceEncodeParams];
  lHTTP.ConnectTimeout := 10000;
  lHTTP.IoHandler := openSSL;
 //esta mierda es super importante, sino no va, sirve para decodificar el flujo de memoria a texto plano en el post
  lHTTP.Request.ContentType := 'application/json;charset=utf-8';
  try
    try
      lHTTP.Post(Url, parametros, str);
      Result := StreamToString(str);
    except
      raise;
    end;
  finally
    lHTTP.Free;
    parametros.Free;
    str.Free;
    openSSL.Free;
  end;
end;

// FUNCI�N INTERNA: HACER UN MD5 DE UN ARRAY DE STRINGS
function getMD5(const Args: array of string): string;
var
  md5: TIdHashMessageDigest5;
  str: string;
  I: Integer;
begin
  for I := 0 to Length(Args) - 1 do
    str := str + Args[I];
  md5 := TIdHashMessageDigest5.Create;
  try
    result := md5.HashStringAsHex(str);
  finally
    md5.Free;
  end;
end;

// FUNCI�N INTERNA: QUITAR NULOS
function RemoveNull(const Input: string): string;
var
  OutputLen, Index: Integer;
  C: Char;
begin
  SetLength(Result, Length(Input));
  OutputLen := 0;
  for Index := 1 to Length(Input) do
  begin
    C := Input[Index];
    if C <> #0 then
    begin
      inc(OutputLen);
      Result[OutputLen] := C;
    end;
  end;
  SetLength(Result, OutputLen);
end;



// -------------------- FUNCIONES EXTERNAS -----------------------------------





// FUNCI�N EXTERNA: INICIAR OBJETO
procedure Datos.init;
begin
	self.ruta := GetCurrentDir;
  ruta_utilidades := ruta + '\utilidades';
	fichero_config := TIniFile.Create(ruta + '\glop.ini');
  ruta_datos := fichero_config.ReadString('BaseDatos', 'servidor_bd', '');


  if LowerCase( ruta_datos ) = 'glop.fdb' then
    ruta_datos := ruta + '\glop.fdb';

  // Comprobar que existe el fdb.
  // Si no existe lanzar error y mensaje.........



  nombre_folder := 'utilidades\cloud';
  ruta_carpeta_nueva := ruta + '\' + nombre_folder;
end;

// FUNCI�N EXTERNA: ABRIR CONEXI�N
procedure Datos.abrirDb;
begin
    baseDatos := TpFIBDatabase.Create(nil);
    with baseDatos do
    begin
      LibraryName := 'fbclient.dll';
      SQLDialect := 3;
      Connected := False;
      maquina := fichero_config.ReadString('BaseDatos', 'maquina', '');
      if maquina = '' then
        maquina := '127.0.0.1';
      DBName := maquina + ':' + ruta_datos;
      ConnectParams.UserName := 'SYSDBA';
      ConnectParams.Password := 'masterkey';
      ConnectParams.CharSet := 'NONE';
      Open;
    end;
  end;

// FUNCI�N EXTERNA: CERRAR CONEXI�N
procedure Datos.cerrarDb;
begin
  baseDatos.CloseDataSets;
  baseDatos.Close;
end;

// FUNCI�N EXTERNA: EJECUTAR SQL
procedure Datos.ejecuta_query(sql:String);
var
  query : TpFIBQuery;
  trans : TpFIBTransaction;
begin
  query := TpFibQuery.Create(nil);
  trans := TpFIBTransaction.Create(nil);
  try
    trans.DefaultDatabase := baseDatos;
    query.Transaction := trans;
    if not(trans.Active) then trans.StartTransaction;
    query.SQL.Text := sql;
    try
      query.ExecQuery;
      trans.Commit;
    except
      trans.Rollback;
      raise
    end;
  finally
    query.Free;
    trans.Free;
  end;
end;

// FUNCI�N EXTERNA: SI EST� GLOP FUNCIONANDO
function Datos.isGlopActive: Boolean;
  var
    ExeFileName: String;
    ContinueLoop: BOOL;
    FSnapshotHandle: THandle;
    FProcessEntry32: TProcessEntry32;
begin
  ExeFileName := 'Glop.exe';
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  Result := False;
  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
    begin
      Result := True;
    end;
    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;


// FUNCI�N EXTERNA: CARGAR LICENCIA
function Datos.cargarLic: Boolean;
var F: file of byte;
  i,posIni: Integer;
  ruta_licencia,texto:String;
  s:byte;
  is_gb: boolean;
begin
  ruta_licencia := ruta + '\glop.lic';
  if not(fileexists(ruta_licencia)) then
  begin
    Result := false;
    Exit;
  end;
  AssignFile(F,ruta_licencia);
  Reset(F);
  posIni := INICIO_SAAS + 1;
  Seek(F, posIni);
  for i := 0 to TAM_SAAS - 1 do
  begin
    Read(F, s);
    texto := texto + chr(s);
  end;
  isSAAS := eliminar_caracteres(desencrip(texto, 1), '*') = '1';

  texto := '';
  posIni := INICIO_MATRICULA + 1;
  Seek(F, posIni);
  for i := 0 to TAM_MATRICULA - 1 do
  begin
    Read(F, s);
    texto := texto + chr(s);
  end;
  licencia := eliminar_caracteres(desencrip(texto, 1), '*');


  texto := '';
  {licencia padre}
  posIni := INICIO_PADRE + 1;
  Seek(F, posIni);
  for i := 0 to TAM_PADRE - 1 do
  begin
    Read(F, s);
    texto := texto + chr(s);
  end;
  GB_Padre := eliminar_caracteres(desencrip(texto, 1), '*');




  // Comprobar si esta licencia es GB o saas: si no es as�, resultado false y no dejamos hacer copia.
  is_gb := false;
  if ( Copy( licencia, 1, 2) = 'GB' ) then
  begin
    is_gb := true;
  end;
                           
  licencia := StringReplace( licencia, 'GB', '', [rfReplaceAll, rfIgnoreCase] );
  licencia := StringReplace( licencia, 'GL', '', [rfReplaceAll, rfIgnoreCase] );
  licencia := StringReplace( licencia, 'MN', '', [rfReplaceAll, rfIgnoreCase] );  
  licencia := StringReplace( licencia, 'AC', '', [rfReplaceAll, rfIgnoreCase] );


  Result := is_gb;
end;

// FUNCI�N EXTERNA: PARAR SERVICIO GLOP
procedure Datos.pararServicioGlop;
begin
  try
    case ServiceGetStatus('GlopService') of
      SERVICE_STOPPED:
        begin

        end;
      SERVICE_START_PENDING:
        begin

        end;
      SERVICE_STOP_PENDING:
        begin

        end;
      SERVICE_RUNNING:
        begin
          pararServicio('GlopService');
        end;
      SERVICE_CONTINUE_PENDING:
        begin

        end;
      SERVICE_PAUSE_PENDING:
        begin

        end;
      SERVICE_PAUSED:
        begin

        end;
    else
      begin

      end;
    end;
  except
    on e: Exception do
  end;
end;

// FUNCI�N EXTERNA: CLONAR DB Y GLOP.INI
function Datos.cloneDb: Boolean;
var
  CommandLine: string;
begin
  GetDosOutput( 'mkdir "' + ruta_carpeta_nueva + '"' );
  ruta_datos_nueva := ruta_carpeta_nueva + '\glop_bk.fdb';

  if not(fileexists(ruta_datos)) then
  begin
    Result := false;
    Exit;
  end;

  CommandLine := GetDosOutput( 'copy /Y "' + ruta_datos + '" "' + ruta_datos_nueva + '"' );
  CommandLine := GetDosOutput( 'copy /Y "' + ruta + '\glop.ini" "' + ruta_carpeta_nueva + '\glop.ini"' );

  ruta_datos := ruta_datos_nueva;

  Result := true;
end;

// FUNCI�N EXTERNA: EMPAQUETAR CARPETA BACKUPS
function Datos.tarBd: Boolean;
begin
  // tar -cvf ttio.tar glop.ini glop_bk.fdb
  GetDosOutput(
    ruta_utilidades + '\tar.exe -cvf "'+ ruta + '\backup.tar" "' + nombre_folder + '\*"',
    ruta
  );
end;

function Datos.unTarBd( nombre: string) : Boolean;
var s: string;
begin

  if not (FileExists( ruta_utilidades + '\' + nombre + '' )) then
    exit;

  s := GetDosOutput(
    'pushd "' + ruta_utilidades + '" && "' + ruta + '\tar.exe" -xf "'+ ruta_utilidades + '\' + nombre + '"',
    ruta_utilidades
  );

end;

// FUNCI�N EXTERNA: GFIX DE UNA BD FIREBIRD
function Datos.gfixBd: Boolean;
var
  base: string;
begin
  // gfix -user SYSDBA -password masterkey -mend -full -ignore glop_bk.fdb.
  GetDosOutput(
    ruta + '\gfix.exe -user SYSDBA -password masterkey -mend -full -ignore "' + ruta_carpeta_nueva + '\glop_bk.fdb"',
    ruta
  );
  // gbak -user SYSDBA -password masterkey -b glop_bk.fdb RAG_GLOP.fbk.
  GetDosOutput(
    ruta + '\gbak.exe -user SYSDBA -password masterkey -b "' + ruta_carpeta_nueva + '\glop_bk.fdb" "' + ruta_carpeta_nueva + '\glop_bk.fbk"',
    ruta
  );

  // Eliminamos copia.
  GetDosOutput(
    'del "' + ruta_carpeta_nueva + '\glop_bk.fdb"',
    ruta
  );
end;

function Datos.copyLocalBd: Boolean;
var copia: string;
begin
  copia := '' + ruta + '\glop_' + formatdateTime('DD_MM_YYYY', now) + '.fdb';
  SysUtils.DeleteFile( copia );
  // gbak -r -user SYSDBA -password masterkey c:\backups\warehouse.fbk dbserver:/db/warehouse2.fdb
  GetDosOutput(
    'copy /Y "' + ruta_datos +'" "' + copia + '"',
    ruta
  );
end;


function Datos.escribirFlag: Boolean;
var
  ruta_flag, ruta_data: string;
  Archivo: TextFile;
begin
  ruta_data := SysUtils.GetEnvironmentVariable('APPDATA');
  ruta_flag := IncludeTrailingPathDelimiter( ruta_data + '\Mobisoft\Glop' ) + 'tagbackup';

  if not DirectoryExists(ruta_data + '\Mobisoft\Glop\') then
    begin
      ForceDirectories(ruta_data + '\Mobisoft\Glop\');
    end;
    try
      AssignFile(Archivo, ruta_flag);
      reWrite(Archivo);
      WriteLn(Archivo, '-');
    finally
      begin
        CloseFile(Archivo);
      end;
    end;

end;

function Datos.launchGlop: Boolean;
var handle: HWND ;
begin
  ShellExecute(handle, 'open', PChar('Glop.exe'), PChar(''), nil, SW_MAXIMIZE);
end;

function Datos.actualizarIni: Boolean;
begin
  fichero_config.WriteString('BaseDatos', 'actualizar', '1');
  
  if not( FileExists( ruta_datos ) ) then
  begin
    // La bd se ha restaurado en el root de glop
    fichero_config.WriteString('BaseDatos', 'servidor_bd', 'glop.fdb');
  end;
end;

function Datos.restoreBd: Boolean;
var rutafinal, salida: string;
begin
  // Verificar que la ruta de servidor_bd de glop.ini existe y accesible.
  // - Caso 1: es accesible continuamos.
  // - Caso 2: NO es accesible:
  // --- Movemos el fbk a la ruta de instalaci�n de glop
  rutafinal := ruta_datos;
  if not( FileExists( ruta_datos ) ) then
  begin
    rutafinal := ruta + '\glop.fdb';
    Writeln( 'No puedo acceder al fdb de la ruta de glop.ini: he colocado el FDB restaurado en la raiz de GLOP, pulse enter para continuar ' );
    ReadLn;
  end;
  // gbak -r -user SYSDBA -password masterkey c:\backups\warehouse.fbk dbserver:/db/warehouse2.fdb
  DeleteFile( PChar( rutafinal ) );
  salida := GetDosOutput(
    'pushd "' + ruta + '" && "' + ruta + '\resetFire.bat"',
    ruta
  );
  salida := GetDosOutput(
    'pushd "' + ruta + '" && "' + ruta + '\gbak.exe" -r -rep -user SYSDBA -password masterkey "' + ruta_carpeta_nueva + '\glopcfg.fbk" "' + rutafinal + '"',
    ruta
  );


  if length( salida ) > 0 then
  begin
  Writeln( 'Error al restaurar copia: ' + salida );
  ReadLn;
  end;
    
  rutafinal := '';
end;

// FUNCI�N EXTERNA: SUBIR BD A GOOGLE
function Datos.saveBackup: Boolean;
  var
    str: TMemoryStream;
    texto: widestring;
    valorAEnviar: widestring;
    backupTar: TFileStream;
    jsonEnvio: TlkJSONobject;
    md5: string;

begin

  backupTar := TFileStream.Create(   ruta + '\backup.tar', fmOpenRead );
  str := TMemoryStream.create;
  str.CopyFrom(backupTar, 0);

  texto := BinToStr(str.Memory, str.Size);
  md5 := getMD5([licencia, 'glop']);
  jsonEnvio := TlkJSONobject.Create;
  jsonEnvio.Add('licencia', licencia);
  jsonEnvio.Add('clave', md5);
  jsonEnvio.Add('operacion', 'save');
  jsonEnvio.Add('file', texto);
  valorAEnviar := TlkJSON.GenerateText(jsonEnvio);
  valorAEnviar := UTF8String(valorAEnviar);
  valorAEnviar := Utf8Encode( RemoveNull(valorAEnviar ) );

  makeCall( URL_ENDPOINT, valorAEnviar );

end;


function Datos.setGlopIni: Boolean;
begin
  if not (FileExists( ruta_carpeta_nueva + '\glop.ini' ) ) then
    exit;

  GetDosOutput( 'copy /Y "' + ruta + '\glop.ini" "' + ruta + '\glop_' + formatdateTime('DD_MM_YYYY', now) + '.ini"' );

  GetDosOutput( 'copy /Y "' + ruta_carpeta_nueva + '\glop.ini" "' + ruta + '\glop.ini"' );
  init();
end;


procedure Datos.listBackup;
var
  str: TMemoryStream;
  texto: widestring;
  valorAEnviar: widestring;
  backupTar: TFileStream;
  jsonEnvio: TlkJSONobject;
  md5: string;            
  log: TextFile;

begin
  md5 := getMD5([licencia, 'glop']);
  jsonEnvio := TlkJSONobject.Create;
  jsonEnvio.Add('licencia', licencia);
  jsonEnvio.Add('clave', md5);
  jsonEnvio.Add('operacion', 'list');
  valorAEnviar := TlkJSON.GenerateText(jsonEnvio);
  valorAEnviar := UTF8String(valorAEnviar);
  valorAEnviar := Utf8Encode( RemoveNull(valorAEnviar ) );

  texto := makeCall( URL_ENDPOINT, valorAEnviar );
  // Guardar tar (texto) en file.
  AssignFile( log, ruta + '\backup2.txt');

  if not (FileExists(ruta + '\backup2.txt')) then
    rewrite(log)
  else
    reset(log);

  append(log);
  WriteLn(log, texto );
  closeFile(log);
  
end;






procedure Datos.getBackup( nombre: string );
var
  str: TMemoryStream;
  texto: widestring;
  valorAEnviar: widestring;
  backupTar: TFileStream;
  jsonEnvio: TlkJSONobject;
  md5: string;
  log: TextFile;

begin
  md5 := getMD5([licencia, 'glop']);
  jsonEnvio := TlkJSONobject.Create;
  jsonEnvio.Add('licencia', licencia);
  jsonEnvio.Add('clave', md5);
  jsonEnvio.Add('operacion', 'get');
  jsonEnvio.Add('nombre', nombre);
  valorAEnviar := TlkJSON.GenerateText(jsonEnvio);
  valorAEnviar := UTF8String(valorAEnviar);
  valorAEnviar := Utf8Encode( RemoveNull(valorAEnviar ) );

  texto := makeCall( URL_ENDPOINT_GLOP, valorAEnviar );
  
  if texto = 'false' then
    exit;


  SysUtils.DeleteFile( ruta_utilidades + '\' + nombre + '' );
  // Guardar tar (texto) en file.
  AssignFile(log, ruta_utilidades + '\' + nombre + '');
  if not (FileExists( ruta_utilidades + '\' + nombre + '' )) then
    rewrite(log)
  else
    reset(log);
  append(log);
  WriteLn(log, texto );
  closeFile(log);
  
end;


procedure Datos.subirFileGordo();
var
  Stream: TStringStream;
  Params: TIdMultipartFormDataStream;
  HTTP  : TIdHTTP;               
  md5: string;  
  openSSL: TIdSSLIOHandlerSocketOpenSSL;
begin
  Stream := TStringStream.Create('');
  HTTP := TIdHTTP.Create();
  try
   Params := TIdMultipartFormDataStream.Create;
   try
    Params.AddFile('file', 'C:\Users\Glop\Documents\glop\Ejecutable\utilidades\cloud\backup_15_1_2021.tar','application/octet-stream');
    md5 := getMD5([licencia, 'glop']);
    Params.AddFormField('licencia', licencia);
    Params.AddFormField('clave', md5);
    Params.AddFormField('operacion', 'save');
    try
      openSSL := TIdSSLIOHandlerSocketOpenSSL.Create(HTTP);
      openSSL.SSLOptions.Method := sslvTLSv1_2;
      openSSL.SSLOptions.SSLVersions := [sslvSSLv2, sslvSSLv3, sslvTLSv1, sslvTLSv1_1, sslvTLSv1_2, sslvSSLv23];
      openSSL.SSLOptions.Mode := sslmUnassigned;
      HTTP.AllowCookies := True;
      HTTP.HTTPOptions := [hoKeepOrigProtocol, hoInProcessAuth, hoForceEncodeParams];
      HTTP.ConnectTimeout := 10000000;
      HTTP.IoHandler := openSSL;
      HTTP.Request.CustomHeaders.AddValue( 'Accept', '*/*' );
      // HTTP.Request.CustomHeaders.AddValue( 'Accept-Encoding', 'gzip, deflate, br' );
      HTTP.Post('https://www.glop.es/google/index.php', Params, Stream);
    except
     on E: Exception do
       WriteLn('Error encountered during POST: ' + E.Message);
    end;
    WriteLn(Stream.DataString);
   finally
    Params.Free;
   end;
  finally
   Stream.Free;
  end;

end;







end.
