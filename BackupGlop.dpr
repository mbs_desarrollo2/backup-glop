program BackupGlop;

{$APPTYPE CONSOLE}

uses
  Windows,
  FIBDatabase,
  pFIBDatabase,
  DB,
  FIBQuery,
  pFIBQuery,
  SysUtils,
  Udatos in 'Udatos.pas' {dmDatos},
  uLkJSON in 'JsonParser\uLkJSON.pas',
  base64 in 'base64.pas';

var
    bd: Datos;
    glopActivo, existeLic: Boolean;
    licencia: string;

procedure hacerBackup();
begin
  // 1.- Verificar glop cerrado y glopService cerrarlo
  bd := Datos.Create;
  bd.init;
  glopActivo := bd.isGlopActive;
  if glopActivo then
  begin
    Exit;
  end;
  // Cargar licencia: si no está el fichero salimos del programa.
  existeLic := bd.cargarLic;
  if not existeLic or not bd.isSaas then
  begin
    Exit;
  end;
  bd.pararServicioGlop;

  // 1.2: Cerrar todas las conexiones a la fdb...

  // 2.- Comprobar que existe FBK: ExtractFilePath(Application.ExeName)+'utilidades\cloud\GlopCfg.fbk

  // 3.- Restaurar la fbk a fdb

  // 4.- Hacer deletes

  // 5.- Hacer FBK de nuevo.

  // 6.- TAR

  if not bd.cloneDb then
  begin
    Exit;
  end;
  // 3.- Preparar bd.
  bd.abrirDb;
  bd.ejecuta_query( 'DELETE FROM tb_tickets_lin_detalle_his' );
  bd.ejecuta_query( 'DELETE FROM tb_tickets_lin_his' );
  bd.ejecuta_query( 'DELETE FROM tb_tickets_his' );
  bd.ejecuta_query( 'DELETE FROM tb_notificaciones_recibidas' );
  bd.ejecuta_query( 'DELETE FROM tb_notificaciones_json' );
  bd.ejecuta_query( 'DELETE FROM tb_notificaciones_enviadas' );
  bd.cerrarDb;

  // 4.- GFIX y GBAK de la bd.
  bd.gfixBd;
  // 5.- TAR de la bd y el glop.ini
  bd.tarBd;
  // 6.- Subir a la API este TAR.
  bd.saveBackup;

end;

procedure restaurarBackup( nombre: string );
begin

  Writeln( 'Restaurando la copia de seguridad, no cierre esta ventana, por favor espere...' );

  // 1.- Obtener la copia de la bd
  // 2.- Verificar glop cerrado y glopService cerrarlo
  bd := Datos.Create;
  bd.init;
  glopActivo := bd.isGlopActive;
  if glopActivo then
  begin
    Exit;
  end;
  // Cargar licencia: si no está el fichero salimos del programa.
  existeLic := bd.cargarLic;
  if not existeLic and not bd.isSaas then
  begin
    Exit;
  end;
  bd.pararServicioGlop;

  bd.getBackup( nombre );
  // bd.listBackup;

  if not (FileExists( bd.ruta_utilidades + '\' + nombre + '' )) then
    exit;
  // Descomprimir .tar
  bd.unTarBd( nombre );
  // Renombrar glop.ini de la instalación de glop
  // Mover el glop.ini de la backup a la ruta de instalación de glop
  bd.setGlopIni();
  // Renombrar el glop.fdb ( a glop_{fecha}.fdb ) original de la ruta datos del glop.ini
  //         o si no es accesible esa ruta la de instalación de glop
  bd.copyLocalBd();
  // Retaurar FBK en esta carpeta.
  bd.restoreBd();
  // actualizar=1 del glop.ini
  bd.actualizarIni();
  
  // Escribir el flag para glop
  bd.escribirFlag();
  // Ejecutar GLOP
  
  bd.launchGlop();
  
  if not( FileExists( bd.ruta_datos ) ) then
  begin
    // La bd se ha restaurado en el root de glop
    // Avisar en la consola.
    Writeln( 'AVISO: La restauracion se ha hecho, pero no se podia alcanzar la ruta de servidor_bd de glop.ini' );
    Writeln( 'y se ha restaurado la copia en la carpeta de GLOP' );
    Writeln( 'Intro para continuar' );
    ReadLn;
  end;
end;

begin
  try

    if ParamCount = 0 then
      hacerBackup
    else
      restaurarBackup( ParamStr( 1 ) );

  except
    on E:Exception do
      Writeln(E.Classname, ': ', E.Message);
  end;



end.
